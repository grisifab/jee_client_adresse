package org.eclipse.controller;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.models.Client;
import org.eclipse.controller.Att;

/**W
 * Servlet implementation class TestServlet
 */
@WebServlet("/CreationClient")
public class CreationClient extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public static ArrayList<Client> listeDeClient= new ArrayList<Client>();
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		this.getServletContext().getRequestDispatcher("/WEB-INF/creerClient.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		boolean test1, test2, test3, test4;
		
		String nom = request.getParameter(Att.ATT_NOM);
		test1 = Att.verifChaine(nom);
		String prenom = request.getParameter(Att.ATT_PRENOM);
		test2 = Att.verifChaine(prenom);
		String telephone = request.getParameter(Att.ATT_TEL);
		test3 = Att.verifNumero(telephone);
		test4 = test1 && test2 && test3;
		
		if(test4) {
			Client client = new Client(nom,prenom,telephone);
			listeDeClient.add(client);
			request.setAttribute(Att.ATT_CLI, client);
		}
		
		this.getServletContext().getRequestDispatcher(test4 ? Att.VUE_CLI : Att.VUE_ERR).forward(request, response);
	}

}
