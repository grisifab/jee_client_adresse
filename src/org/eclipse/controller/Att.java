package org.eclipse.controller;

public class Att {
	public static final String ATT_NOM = "nom";
	public static final String ATT_PRENOM = "prenom";
	public static final String ATT_TEL = "telephone";
	public static final String ATT_RUE = "nomRue";
	public static final String ATT_CP = "codePostal";
	public static final String ATT_ADR = "adresse";
	public static final String ATT_CLI = "client";
	public static final String VUE_ADR = "/WEB-INF/afficherAdresse.jsp";
	public static final String VUE_CLI = "/WEB-INF/afficherClient.jsp";
	public static final String VUE_ERR = "/WEB-INF/afficherErreur.jsp";
	
	
	public static boolean verifChaine(String s) {

		// test existance et longueur
		if (s == null || s.length() < 2)
			return false;

		// test majuscule
		char c = s.charAt(0);
		if (!(c >= 'A' && c <= 'Z'))
			return false;

		// test caractères
		for (int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z'))
				return false;
		}
		
		return true;
	}
	
	public static boolean verifNumero(String s) {
		// test existance et longueur
		if (s == null || s.length() < 2)
			return false;

		// test chiffres
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (!(c >= '0' && c <= '9'))
				return false;
		}
		return true;
	}
	
}
