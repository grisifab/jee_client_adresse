package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AfficherTotal
 */
@WebServlet("/AfficherTotal")
public class AfficherTotal extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficherTotal() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		String chaine_client = new String();
		
		for (int i = 0; i < CreationClient.listeDeClient.size(); i++) {
			chaine_client += CreationClient.listeDeClient.get(i).toString();
			chaine_client += "\n";
		}
		
		String chaine_adresse = new String();
		
		for (int i = 0; i < CreationAdresse.listeDeAdresse.size(); i++) {
			chaine_adresse += CreationAdresse.listeDeAdresse.get(i).toString();
			chaine_adresse += "\n";
		}
		
		
		request.setAttribute("List_client", chaine_client);
		request.setAttribute("List_adresse", chaine_adresse);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/afficherTotal.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
