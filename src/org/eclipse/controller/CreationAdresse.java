package org.eclipse.controller;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.models.*;
import org.eclipse.controller.Att;

@WebServlet("/CreationAdresse")
public class CreationAdresse extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	public static ArrayList<Adresse> listeDeAdresse= new ArrayList<Adresse>();     
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {				
		this.getServletContext().getRequestDispatcher("/WEB-INF/creerAdresse.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		boolean test1, test2, test3, test4, test5, test6;
		String nom = request.getParameter(Att.ATT_NOM);
		test1 = Att.verifChaine(nom);
		String prenom = request.getParameter(Att.ATT_PRENOM);
		test2 = Att.verifChaine(prenom);
		String telephone = request.getParameter(Att.ATT_TEL);
		test3 = Att.verifNumero(telephone);
		String nomRue = request.getParameter(Att.ATT_RUE);
		test4 = Att.verifChaine(nomRue);
		String codePostal = request.getParameter(Att.ATT_CP);
		test5 = Att.verifNumero(codePostal);
		test6 = test1 && test2 && test3 && test4 && test5;
		
		if(test6) {
			Client client = new Client(nom,prenom,telephone);
			Adresse adresse = new Adresse(nomRue, codePostal, client);
			listeDeAdresse.add(adresse);
			request.setAttribute(Att.ATT_ADR, adresse);
		}
			
		this.getServletContext().getRequestDispatcher(test6 ? Att.VUE_ADR : Att.VUE_ERR).forward(request, response);
	}

}
