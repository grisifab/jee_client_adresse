package org.eclipse.models;

public class Adresse {
	
	private String nomRue;
	private String codePostal;
	private Client client;
	public String getNomRue() {
		return nomRue;
	}
	public void setNomRue(String nomRue) {
		this.nomRue = nomRue;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	@Override
	public String toString() {
		return "Adresse [nomRue=" + nomRue + ", codePostal=" + codePostal + ", client=" + client + "] ";
	}
	public Adresse(String nomRue, String codePostal, Client client) {
		super();
		this.nomRue = nomRue;
		this.codePostal = codePostal;
		this.client = client;
	}
	public Adresse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
